@extends('layouts.template')
@section('title', 'My Notes')
@section('content')

                <div class="container">
                <h1 class="text-center py-4 io-text">My Notes</h1>
                    <div class="d-flex justify-content-center">
                        <form method="get" action="/talent-add-notes">
                        <!-- <i style='font-size:25px' class='far my-3' id="add-note">&#xf271;</i>   -->
                                                   
                        <img src="{{ URL::to('/assets/img/notes-icon.png') }}" class="note-icon">                                             

                        <button type="submit" class="btn btn-info text-center my-2">Post Notes</button>
                        </form>

                    </div>    


                </div>
      
    <div class="container my-4" id="indexContainer">
        <div class="d-flex justify-content-center">
            <div class="col-lg-10">
           
            <table class="table ">
                <thead>
                    <tr>
                        <th>Title</th>             
                        <th>Date</th>  
                    </tr>
                </thead>

                <tbody>               
                @foreach($talentnotes as $note)
                @auth
                @if(Auth::user()->id === $note->user_id)
                    <tr class="jobs-tr">
                        <td>{{$note->title}}</td>
                        <td>{{$note->created_at->isoFormat('dddd, MMMM Do YYYY')}}</td>                
                        <td>

                            <div class="d-flex">

                            <!-- <a href="/talent-notes-update/{{$note->id}}" class="btn btn-info mx-2">Update</a> -->

                            <a href="/talent-notes-update/{{$note->id}}" class="btn"><i style='font-size:24px' class='far' id="update-note">&#xf274;</i></a>
                           
                            <!-- <a href="/delete-talent-note/{{ $note->id }}" class="btn btn-danger mr-3">Delete</a>                             -->
                            

                            <!-- Button trigger modal -->
                        <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"> Delete </button> -->
                        <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"> <i style="font-size:24px" class="fa mx-2" id="delete-note" >&#xf00d;</i> </button>


                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-center">
                                        Are you sure to delete this note?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <a href="/delete-talent-note/{{ $note->id }}" class="btn btn-danger mr-3" id="">Delete</a>
                                          
                                    </div>
                                    </div>
                                </div>
                                </div>

                            </div>

                        </td>
                    </tr> 
                @endif 
                @endauth
                @endforeach              
                </tbody>
               
          
            </table>
                   
            <div>

                        
                </div>                
            </div>                                                              
        </div>                    
    </div>


@endsection