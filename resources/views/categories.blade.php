@extends('layouts.template')
@section('title', 'Categories')
@section('content')


    <h1 class="py-5 text-center">Category</h1>
    <div class="container pt-1">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <form action="/add-category" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Category:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-info">Add Category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-3 text-center">All Categories</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                @foreach($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name}}</td>
                        
                        <td>
                            <div class="d-flex">
                                <a href="/update-category/{{$category->id}}" class="btn btn-info mr-3">Update Now</a>
                                
                                <form action="/delete-category" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="hidden" name="category_id" value="{{$category->id}}">
                                    <button type="submit" class="btn btn-danger">Delete</button>
                                </form>
                                
                                        <!-- Button trigger modal -->
                                        <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                        Erase
                                        </button> -->

                                        <!-- Modal -->
                                        <!-- <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel"></h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-center">
                                                Are you sure to delete this category?
                                            </div>
                                            <div class="modal-footer">
                                                    <form action="/delete-category" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                        <input type="hidden" name="category_id" value="{{$category->id}}">
                                                        <button type="submit" class="btn btn-danger">Delete</button>
                                                    </form>
                                            </div>
                                            </div>
                                        </div>
                                        </div> -->
                                
                            </div>    
                        </td>
                    </tr>
                </tbody>


                @endforeach
            </table>
        </div>
    </div>


@endsection