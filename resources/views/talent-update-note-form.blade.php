@extends('layouts.template')
@section('title', 'Talent Update Note')
@section('content')
    <h1 class="text-center py-5">Talent Update Note</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
        
                <form action="/talent-update-note/{{$talentnotes->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="jobtitle">Title</label>
                        <input type="text" name="title" value="{{$talentnotes->title}}"class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="jobdescription">Summary</label>
                        <input type="text" name="content" value="{{$talentnotes->content}}" class="form-control">
                    </div>
                  
                    <div class="text-center">
                        <button class="btn btn-info">Update Note</button>
                    </div>
                </form>
           
            </div>
        </div>
    </div>
@endsection