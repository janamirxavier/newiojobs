@extends('layouts.template')
@section('title', 'Feedback')
@section('content')
    <h1 class="text-center py-5">Feedback</h1>
    <div class="container">
        <div class="row">
         
        <div class="col-lg-6 offset-lg-3">
            <div class="app-form card-body margin-bottom" id="apply-job-card">


            

    
            <form action="" method="post">
            @csrf
                <div class="container">
                        <!-- <h1>
                            Gmail Sender
                        </h1> -->
                    
                        @if(session('success'))
                            <b>{{session('success')}}</b>
                        @endif
                        <input  class="form-control my-2" name="email" type="email" placeholder = "Email to..." /> 
                        <input  class="form-control my-2" name="subject" type="text" placeholder = "Subject..." /> 
                        <textarea class="form-control" name="content" placeholder = "Your email..." rows="6"></textarea>

                
                       <div class="text-center">
                        <button class="btn btn-info my-2">Send</button>
                    </div>
                </div>
            </form>        
        </div>
    	</div>

         
         </div>  
    </div>
@endsection