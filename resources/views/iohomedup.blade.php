<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>@yield('title')</title>

{{--    bootswatch --}}
    <link rel="stylesheet" href="https://bootswatch.com/4/spacelab/bootstrap.css">
    <link rel="stylesheet" href="/css/styles.css">
    

</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/file') }}">
                    IOjobs
                </a>
            </div>
        </nav>
    </div>
    
    <main class="py-4" id="indexContainer">
        
    <div class="jumbotron jumbotron-fluid col-md-12 mr-auto" id="main-jumbo">				  
	    <h1 class="display-4 text-center" id="jumbo-text">Connecting clients and talents online PH</h1>
		<p class="lead text-center">Bacon ipsum dolor amet pig jowl pork andouille spare ribs chuck ham pork chop short loin pancetta shank....</p>				  
	</div>
    
        <section class="d-flex justify-content-center align-items-center flex-column">
        
			<div class="card-deck">
				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/motivation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Community</h5>
				    </div>
                </div>


                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/bulb.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Innovation</h5>
				    </div>
                  </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/presentation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Integrity</h5>
				    </div>
				  </div>
                </div>
                
		</section>
    </main><div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/file') }}">
                    IOjobs
                </a>
            </div>
        </nav>
    </div>
    
    <main class="py-4" id="indexContainer">
        
    <div class="jumbotron jumbotron-fluid col-md-12 mr-auto" id="main-jumbo">				  
	    <h1 class="display-4 text-center" id="jumbo-text">Connecting clients and talents online PH</h1>
		<p class="lead text-center">Bacon ipsum dolor amet pig jowl pork andouille spare ribs chuck ham pork chop short loin pancetta shank....</p>				  
	</div>
    
        <section class="d-flex justify-content-center align-items-center flex-column">
        
			<div class="card-deck">
				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/motivation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Community</h5>
				    </div>
                </div>


                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/bulb.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Innovation</h5>
				    </div>
                  </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/presentation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Integrity</h5>

				    </div>
				  </div>
                </div>
                
		</section>
    </main>
  
<!-- end of app        -->
</div> 

    <footer class="bg-primary" id="footer-part">
            <div class="d-flex justify-content-center">
                <p class="my-5 text-white">IOjobs © 2020 </p>
            </div>
    </footer>
</body>
</html>