@extends('layouts.template')
@section('title', 'Applicant Profile')
@section('content')
    <h1 class="py-4 text-center">My Profile</h1>
    <div class="container">
        <div class="row">
       
        
        <div class="col-lg-6 offset-lg-3 card-body" id="profilecard">
                <form action="/applicant-update-profile/" method="POST" enctype="multipart/form-data">
                    @csrf
                    @auth
                    <!-- <input type="hidden" name="id" value="{{ Auth::user()->id}}"> -->
                    <p class="card-text">Name: {{ Auth::user()->name }}</p>
                   <input type="hidden" name="id" value="{{$profile->id}}">
            
               

                    <div class="form-group">
                        <label for="jobtitle">Address</label>
                        <input type="text" name="address" class="form-control" value="{{$profile->address}}">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Contact</label>
                        <input type="text" name="contact" value="{{$profile->contact}}"class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Professional Links</label>
                        <input type="text" name="links" value="{{$profile->links}}"class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Summary</label>
                        <input type="text" name="summary" value="{{$profile->summary}}"class="form-control">
                        <!-- <textarea type="text" name="summary" value="summary"class="form-control" ></textarea> -->

                    </div>

                    
                
                    <div class="text-center">
                        <button class="btn btn-info">Update</button>
                       
                    </div>
                    @endauth
                </form>

            </div>
         
        </div>
    </div>    

@endsection