@extends('layouts.template')
@section('title', 'Applicant Profile')
@section('content')
    <h1 class="py-4 text-center">My Profile</h1>
    <div class="container">
        <div class="row">
       
        
            <div class="col-lg-4 offset-lg-4 card-body" id="profilephotoupdate">
                <form action="/applicant-update-photo/" method="POST" enctype="multipart/form-data">
                    @csrf
                    @auth
                    <!-- <input type="hidden" name="id" value="{{ Auth::user()->id}}"> -->
                    <p class="card-text">Name: {{ Auth::user()->name }}</p>
                   <input type="hidden" name="id" value="{{$profile->id}}">
            
                    <div class="form-group">
                        <label for="jobtitle">Profile Image</label>
                        <input type="file" name="imgPath" value="{{$profile->address}}"class="form-imgPath">
                    </div>                    
                
                    <div class="text-center">
                        <button class="btn btn-info">Update</button>
                       
                    </div>
                    @endauth
                </form>

            </div>
         
        </div>
    </div>    

@endsection