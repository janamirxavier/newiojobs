@extends('layouts.template')
@section('title', 'Update Category')
@section('content')
    <h1 class="text-center py-5">Update Category </h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
                <div class="card-update card-body margin-bottom" id="job-update-form">
                    <form action="/update-category/{{$categories->id}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')                   

                        <div class="text-center">
                            <label for="jobtitle" id="jobtitle-admin">Category: </label>
                            <input type="text" name="name" value="{{$categories->name}}"class="form-control">
                        </div>

                        <div class="text-center">
                        <button class="btn btn-info my-4">Update Category</button>
                        </div>
                
                    </form>
                    
                </div>
            </div>
        </div>
    </div>
@endsection