@extends('layouts.template')
@section('title', 'Add Jobs')
@section('content')


    <h1 class="text-center py-3">Post Jobs</h1>

<div class="container">
<div class="row">    
<div class="col-lg-6 offset-lg-3 card-body" id="profilecard">
            <div class="card-update card-body margin-bottom" id="job-update-form">                
                    <form action="/add-jobs"  method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="jobtitle">Client Name/ Company:</label>
                            <input type="text" name="client" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="jobtitle">Job Title:</label>
                            <input type="text" name="jobtitle" class="form-control">
                        </div>
                        <div class="form-group" id="job-des">
                            <label for="jobdescription">Job Description:</label>
                            <input type="text-area" name="jobdescription" class="form-control">
                        </div>
                        <div class="form-group" id="">
                            <label for="salary">Salary:</label>
                            <input type="number" name="salary" class="form-control">
                        </div>
                        <div class="form-group" id="">
                            <label for="country">Country:</label>
                            <input type="text" name="country" class="form-control">
                        </div>                      

                        <div class="form-group">
                            <label for="category_id">Category</label>
                            <select name="category_id" class="form-control">
                                @foreach($categories as $category)
                                    <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="text-center">
                            <button class="btn btn-info">Post Job</button>
                        </div>
                    </form>   
            </div>
        </div>
</div>
</div>

@endsection