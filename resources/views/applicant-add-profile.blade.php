@extends('layouts.template')
@section('title', 'Applicant Profile')
@section('content')
    <h1 class="py-3 text-center">My Profile</h1>
    <div class="container">
        <div class="row">
       
            <div class="col-lg-4 offset-lg-4 card-body">
                <form action="/applicant-add-profile" method="POST" enctype="multipart/form-data">
                @csrf
                @auth
                    <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                    <p class="card-text">Name: {{Auth::user()->name}}</p>
                @endauth
                    <div class="form-group">
                        <label for="jobtitle">Address</label>
                        <input type="text" name="address" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Contact</label>
                        <input type="text" name="contact" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Professional Links</label>
                        <input type="text" name="links" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Summary</label>
                        <input type="text" name="summary" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Image</label>
                        <input type="file" name="imgPath" class="form-control">
                    </div>
                
                    <div class="text-center">
                        <button class="btn btn-info">Submit</button>
                    </div>
                </form>
            </div>
                    
                
        </div>           
                    
    </div>    

@endsection