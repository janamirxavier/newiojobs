@extends('layouts.template')
@section('title', 'Update Job')
@section('content')
    <h1 class="text-center py-5">Update Profile </h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
            <div class="card-update card-body margin-bottom" id="job-update-form">
                <form action="/applicant-edit-profile" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="text-center">
                        <p class="card-text">Name: {{ Auth::user()->name }}</p>
                        
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Job Title</label>
                        <input type="text" name="jobtitle" value=""class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="jobdescription">Description</label>
                        <input type="text" name="jobdescription" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary</label>
                        <input type="number" name="salary" value="" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" name="country" value="" class="form-control">
                    </div>

                    <div class="text-center">
                        <button class="btn btn-info">Update Job</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection