@extends('layouts.template')
@section('title', 'Job Posts')
@section('content')

                <div class="container">
                <h1 class="text-center my-4 io-text">My Notes </h1>
                    <div class="d-flex justify-content-center">
                        <form method="get" action="/applicant-add-notes">
                            <button type="submit" class="btn btn-info text-center my-4">Add Notes</button>
                        </form>
                    </div>    
                </div>
      
    <div class="container" id="indexContainer">
        <div class="d-flex justify-content-center">
            <div class="col-lg-10">
           
            <table class="table table-striped">
                <thead>
                    <tr>
                        <!-- <th>ID</th>
                        <th>JobTitle</th>
                        <th>Client</th> -->
                        <th>Title</th>
                        <th>Posted</th>
                        
                    </tr>
                </thead>


                <tbody>                
                @foreach($applicantnotes as $note)
                @auth
                @if(Auth::user()->id === $note->user_id)
                    <tr class="jobs-tr">
                        <td>{{$note->title}}</td>
                        <td>{{$note->content}}</td>                        
                        <td>
                            <div class="d-flex">                           
                            <a href="" class="btn btn-info mr-3">Update</a>                            
                            <a href="" class="btn btn-danger">Delete</a>
                        </td>
                    </tr> 
                @endif 
                @endauth
                @endforeach
                </tbody>
            
          
            </table>
                   
                   
            <div>

                        
                </div>                
            </div>                                                              
        </div>                    
    </div>


@endsection