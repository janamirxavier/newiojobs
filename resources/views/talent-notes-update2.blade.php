@extends('layouts.template')
@section('title', 'Talent Update Note')
@section('content')
    <h1 class="text-center py-4">My Notes</h1>
    <div class="container">
        <div class="row">

        <div class="col-lg-6 offset-lg-3 card-body" id="profilecard">  
    
                <form action="/talent-notes-update2/{{$talentnotes->id}}" method="POST" enctype="multipart/form-data">
                @csrf
                    @method('PATCH')

                    <div class="form-group py-3">
                        <label for="jobtitle">Title:</label>
                        <input type="text" name="title" value="{{$talentnotes->title}}"class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobdescription">Summary:</label>
                        <input type="textarea" name="content" value="{{$talentnotes->content}}" id="notes" class="form-control">
                        
                    </div>
                  
                    <!-- <div class="form-group">
                        <label for="jobtitle">Summary</label>
                        <textarea class="form-control" name="content" value="{{$talentnotes->content}}" rows="5"></textarea>
                    </div> -->

                    <div class="text-center">
                        <button a href="/talent-update-note/{id}" class="btn btn-info">Update Note</button>                       
                    </div>

                    

                </form>
           
            </div>
        </div>
    </div>


    </script>

@endsection