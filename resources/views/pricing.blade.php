@extends('layouts.front-template')
@section('title', 'Pricing')
@section('content')
    	
</div>    
        <section class="d-flex justify-content-center align-items-center flex-column">
		<h1 class="display-4 text-center py-4" id="jumbo-text">Pricing</h1>
			<div class="card-deck py-4">
				    <div class="card" id="pricing-cards">
                    <div class="card-header text-center" id="pricing-price">FREE</div>
				    <div class="card-body"><br>
				      
                      <h5 class="card-title " id="pricing-des">Up to 3 Job Posts</h5><br>
                      <h5 class="card-title" id="pricing-des"> 2 days Job Post approval</h5><br>
                      <h5 class="card-title" id="pricing-des"> View Job Applications</h5><br>
				    </div>
                    </div>
                  
				    <div class="card" id="pricing-cards">
                    <div class="card-header text-center" id="pricing-price">PRO $69/month</div>

                        <div class="card-body"><br>
                        <h5 class="card-title " id="pricing-des">Up to 3 Job Posts</h5><br>                                     
                        <h5 class="card-title" id="pricing-des"> Instant Job Post approval</h5><br>
                        <h5 class="card-title" id="pricing-des"> View Job Applications</h5><br>
                        <h5 class="card-title" id="pricing-des"> Communicate With Talents</h5><br>
                        <h5 class="card-title" id="pricing-des"> Contact 75 Talents/month</h5>
                        </div>
                    </div>
                  
				  <div class="card" id="pricing-cards">
                    <div class="card-header text-center" id="pricing-price">PREMIUM $99/month</div>

                        <div class="card-body"><br>
                        <h5 class="card-title " id="pricing-des">Up to 10 Job Posts</h5><br>                                   
                        <h5 class="card-title" id="pricing-des"> Instant Job Post approval</h5><br>
                        <h5 class="card-title" id="pricing-des"> View Job Applications</h5><br>
                        <h5 class="card-title" id="pricing-des"> Communicate With Talents</h5><br>
                        <h5 class="card-title" id="pricing-des"> Contact 500 Talents/month</h5>
                    </div>
				  </div>				 
			    
            </div>
			
		</section>


@endsection