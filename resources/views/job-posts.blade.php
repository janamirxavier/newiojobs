@extends('layouts.template')
@section('title', 'Job Posts')
@section('content')

                <div class="container">
                    <h1 class="text-center my-4 io-text">Admin Job Posts</h1>
                    <div class="d-flex justify-content-center">
                        <!-- <form method="get" action="/add-jobs">
                            <button type="submit" class="btn btn-info text-center my-4">Add New Job</button>
                        </form> -->
                        <form method="get" action="/add-vip-jobs">
                            <button type="submit" class="btn btn-info text-center my-4 mx-2">Add Job</button>
                        </form>
                    </div>    
                </div>
      
    <div class="container">
        <div class="d-flex justify-content-center">
            <div class="col-lg-10">
           
            <table class="table table-striped">
                <thead>
                    <!-- <tr>
                        <th>ID</th>
                        <th>JobTitle</th>
                        <th>Country</th>
                        <th>Posted</th>
                        
                    </tr> -->
                </thead>
                <tbody>
                @foreach($jobs as $job)
                    <tr class="jobs-tr">
                        <td>{{$job->id}}</td>
                        <td>{{$job->jobtitle}}</td>
                        <td> {{$job->country}} </td>
                        <td> {{$job->created_at}} </td>
                        <td>

                            <div class="d-flex">
                            <a href="/update-job/{{ $job->id }}" class="btn btn-info mr-3">Update</a>
                            <form action="/delete-job" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="job_id" value="{{$job->id}}">                             
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            </div>

                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>

            <h1 class="text-center my-4 io-text">Active Job Posts</h1>
               
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Client</th>
                        <th>JobTitle</th>
                        <th>Country</th>
                        <th>Posted</th>
                        
                    </tr>
                </thead>
                <tbody>
                @foreach($clientjobs as $job)
                    <tr class="jobs-tr">
                        <td>{{$job->id}}</td>
                        <td>{{$job->client}}</td>
                        <td>{{$job->jobtitle}}</td>
                        <td> {{$job->country}} </td>
                        <td> {{$job->created_at}} </td>
                        <td> {{$job->vip}} </td>
                        <td>

                            <div class="d-flex">
                            <a href="/update-job/{{ $job->id }}" class="btn btn-info mr-3">Update</a>
                            <form action="/delete-job-admin" method="POST">
                                @csrf
                                
                                <input type="hidden" name="job_id" value="{{$job->id}}">                             
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                            </div>

                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>

            <div>

            </div>                
        </div>                                                              
    </div>                    
</div>


@endsection