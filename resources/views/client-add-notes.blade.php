@extends('layouts.template')
@section('title', 'My Notes')
@section('content')


    <h1 class="text-center py-4">My Notes</h1>

<div class="container">
<div class="row">    
    
<div class="col-lg-6 offset-lg-3 card-body" id="profilecard">           
                    <form action="/client-add-notes"  method="POST">
                        @csrf
                        <div class="form-group py-3">
                            <label for="jobtitle">Title:</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                      

                        <div class="form-group">
                        <label for="jobtitle">Summary:</label>
                        <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="8"></textarea>
                        </div>


                        <div class="text-center">
                            <button class="btn btn-info">Save Note</button>
                        </div>
                    </form>   
            </div>
       
</div>
</div>

@endsection