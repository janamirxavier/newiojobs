@extends('layouts.template')
@section('title', 'Update Job')
@section('content')
    <h1 class="text-center py-5">My Jobs </h1>
    <div class="container">
        <div class="row">
        <div class="col-lg-6 offset-lg-3 card-body" id="clientprofilecard">
            
                <form action="/client-update-job/{{$clientjobs->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')

                    <div class="text-center">
                        <label for="jobtitle"id="jobid" >Job ID: {{$clientjobs->id}}</label>
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Client</label>
                        <input type="text" name="client" value="{{$clientjobs->client}}"class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="jobtitle">Job Title</label>
                        <input type="text" name="jobtitle" value="{{$clientjobs->jobtitle}}"class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="jobdescription">Description</label>
                        <input type="text" name="jobdescription" value="{{$clientjobs->jobdescription}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary</label>
                        <input type="number" name="salary" value="{{$clientjobs->salary}}" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="country">Country</label>
                        <input type="text" name="country" value="{{$clientjobs->country}}" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id" class="form-control">
                            @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-info">Update Job</button>
                    </div>
                </form>
          
            </div>
        </div>
    </div>
@endsection