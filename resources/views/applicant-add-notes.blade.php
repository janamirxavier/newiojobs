@extends('layouts.template')
@section('title', 'Add Jobs')
@section('content')


    <h1 class="text-center py-3">My Notes</h1>

<div class="container">
<div class="row">    
    <div class="col-lg-4 offset-lg-4">
            <div class="card-update card-body margin-bottom" id="job-update-form">                
                    <form action="/applicant-add-notes"  method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="jobtitle">Title:</label>
                            <input type="text" name="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="jobtitle">Summary</label>
                            <input type="text" name="content" class="form-control">
                        </div>

                        <div class="text-center">
                            <button class="btn btn-info">Save Note</button>
                        </div>
                    </form>   
            </div>
        </div>
</div>
</div>

@endsection