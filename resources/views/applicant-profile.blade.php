@extends('layouts.template')
@section('title', 'Applicant Profile')
@section('content')
    <h1 class="py-4 text-center">My Profile</h1>
    <div class="container">
        <div class="row">
        
        <div class="col-lg-6 offset-lg-3 card-body" id="profilecard">
            @if($count > 0)     
            @foreach($profiles as $profile)      
            <div class="d-flex justify-content-center my-2 image-container2">           
               <img src="{{$profile->imgPath}}"  height="200px" alt="">
            </div>
                <!-- <div class="update-photo text-center my-2"> -->
                    <a href="/applicant-update-photo/{{$profile->id}}" >
                    <h3 class="update-photo text-center my-3" >Update Photo</h3>
                    </a>
                <!-- </div> -->

              
                <div class="my-4">  
               
                    <p>Name: {{$profile->user->name}}</p>
                    <p>Address: {{$profile->address}}</p>
                    <p>Contact: {{$profile->contact}}</p>
                    <p>Professional Links: <a href="{{$profile->links}}" target=”_blank” >{{$profile->links}}</a> </p>
                    <p>Summary: {{$profile->summary}}</p>
                </div>
                
                <div class="text-center">
                    <a href="/applicant-update-profile/{{$profile->id}}" class="btn btn-info justify-content-center">Update Profile</a>
                </div>
            @endforeach
            @else               
                <p class="text-center">You don't have an existing profile.</p>
                <p class="text-center">Update your profile now to start getting clients.</p>
                
                <div class="text-center">
                    <a href="/applicant-add-profile" class="btn btn-info justify-content-center">Update Profile</a>
                </div>            
            @endif
                           
        </div>                                
    </div>    
</div>
@endsection