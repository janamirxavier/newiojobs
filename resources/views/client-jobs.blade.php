@extends('layouts.template')
@section('title', 'Jobs')
@section('content')

<h1 class="text-center py-3 io-text">Jobs</h1>
<div class="container">
        <div class="row">
                    <div class="col-lg-2">
                        <!-- <h4 class="text-center filter-text">Filter</h4> -->
                        <div class="list-group">
                                <a href="/client-jobs" class="all-filter text-center" >All</a>
                            @foreach($categories as $category)
                                <a href="/client-jobs?category_id={{$category->id}}" class="list-group-item list-group-item-action" id="categories">{{$category->name}}</a>
                            @endforeach
                        </div>
                    </div>    

                    <div class="col-lg-8" >                                                                       
                                    @foreach($clientjobs as $job)                                    
                                        <div class="card border border-light">                                           
                                            <div class="card-body margin-bottom">

                                            <!-- <p class="card-text d-flex justify-content-center" id="vip">VIP</p> -->
                                            
                                            <!-- <p class="card-text text-center" id="vip">{{$job->vip}}</p> -->
                                            
                                          
                                            <h3 class="card-title text-center my-3" >{{$job->jobtitle}}</h3>
                                         

                                                <p class="card-text">Client: {{$job->client}}</p>
                                                <p class="card-text">Description: {{$job->jobdescription}}</p>
                                                <p class="card-text">Salary: {{$job->salary}}</p>
                                                <p class="card-text">Country: {{$job->country}}</p>
                                                <p class="card-text">Category: {{$job->category->name}}</p>
                                                <p class="card-text">Posted: {{$job->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}}</p>

                                                
                                              
                                                
                                            </div>
                                        </div>                                        
                                    @endforeach  
                                </div>
                                                           
                    <div class="col-lg-2">                    
                        <h5 class="text-center filter-text">Hi, {{Auth::user()->name}}!</h5>                    
                        @auth  
                        

                        <div class="list-group-item list-group-item-action" id="app-options">
                            <h5 class="text-center"><a href="/client-notes" id="talentnotes">My Notes</a></h5>
                        </div> 

                      
                        @endauth                       
                    </div>                                    
    </div>                                         
</div>
@endsection