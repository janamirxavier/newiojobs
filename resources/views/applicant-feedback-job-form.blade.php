@extends('layouts.template')
@section('title', 'Feedback')
@section('content')
    <h1 class="text-center py-5">Feedback</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
            <div class="app-form card-body margin-bottom" id="apply-job-card">

                    @csrf
                    @method('PATCH')
                    
                    <div class="form-group">
                        <h3 class="card-title text-center my-5" >{{$clientjobs->jobtitle}}</h3>
                        
                    </div>
                    
                    <div class="form-group">
                        <label for="jobtitle">Client: {{$clientjobs->client}}</label>
                        
                    </div>
                    <div class="form-group">
                        <label for="jobdescription">Description: {{$clientjobs->jobdescription}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary: {{$clientjobs->salary}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="country">Country: {{$clientjobs->country}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="category">Category: {{$clientjobs->category->name}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="category">Posted: {{$clientjobs->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}}</label>                       
                    </div>



                
                        <form action="/test-mail" method="POST" enctype="multipart/form-data">

                        @csrf
                       
                        
                            <div class="form-group">
                                    <!-- <h1>
                                        Gmail Sender
                                    </h1> -->
                                
                                    @if(session('success'))
                                        <b>{{session('success')}}</b>
                                    @endif
                                    <input  class="form-control my-3" name="email" type="email" value="janamirxavier@gmail.com" id="d-fields" placeholder = "Email to..." /> 
                                    <input  class="form-control my-3" name="subject" type="text" value="job-post feedback" id="d-fields" placeholder = "Subject..." /> 
                                    <textarea class="form-control my-2" name="content" placeholder = "" rows="6"></textarea>

                            
                                    <div class="text-center">
                                        <button class="btn btn-info my-2">Send</button>
                                    </div>

                            </div>
                        </form>


                    
            </div>
            </div>
        </div>
    </div>

   
    
@endsection