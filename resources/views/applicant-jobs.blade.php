@extends('layouts.template')
@section('title', 'Jobs')
@section('content')

<h1 class="text-center py-3 io-text">Jobs</h1>

<div class="container">
        <div class="row">
                    <div class="col-lg-2">
                        <!-- <h4 class="text-center filter-text">Filter</h4> -->
                        <div class="list-group">
                                <a href="/applicant-jobs" class="all-filter text-center">All</a>
                            @foreach($categories as $category)
                                <a href="/applicant-jobs?category_id={{$category->id}}" class="list-group-item list-group-item-action" id="categories">{{$category->name}}</a>
                            @endforeach
                        </div>
                    </div>     
                                    
                                <div class="col-lg-8" >                                                                       
                                    @foreach($clientjobs as $job)                                    
                                        <div class="card border border-light">                                           
                                            <div class="card-body margin-bottom">

                                            <!-- <p class="card-text d-flex justify-content-center" id="vip">VIP</p> -->
                                            
                                            <!-- <p class="card-text text-center" id="vip">{{$job->vip}}</p> -->
                                            
                                            <a href="/apply-client-job/{{ $job->id }}">
                                          
                                            <h3 class="card-title text-center my-3" >{{$job->jobtitle}}</h3>
                                            </a>
                                                <p class="card-text">Client: {{$job->client}}</p>
                                                <p class="card-text">Description: {{$job->jobdescription}}</p>
                                                <p class="card-text">Salary: {{$job->salary}}</p>
                                                <p class="card-text">Country: {{$job->country}}</p>
                                                <p class="card-text">Category: {{$job->category->name}}</p>
                                                <p class="card-text">Posted: {{$job->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}}</p>
                                                
                                                <div class="d-flex justify-content-center report-note" style="float:right">
                                                    <a href="/applicant-feedback-job-new/{{ $job->id }}">
                                                    <img src="{{ URL::to('/assets/img/mega7.png') }}">
                                                    </a>
                                                    
                                                    <!-- <a href="" style="float:right" class="mx-1" >
                                                    <img src="{{ URL::to('/assets/img/bookmark3.png') }}">
                                                    </a> -->

                                                    <!-- <div id="myDiv">
                                                        <input id="info" type="button" value="{{ $job->id }}" class="showButon" />
                                                    </div> -->



                                                </div>

                                                
                                                
                                            </div>
                                        </div>                                        
                                    @endforeach  
                                </div>
                                                           
                    <div class="col-lg-2">                    
                        <h5 class="text-center filter-text">{{Auth::user()->name}}</h5>                    
                        @auth  
                        <div class="d-flex justify-content-center my-2 image-container">
                        <img src="{{Auth::user()->profile->imgPath}}">
                        </div>

                        <div class="list-group-item list-group-item-action" id="app-options">
                            <h5 class="text-center"><a href="/talent-notes" id="talentnotes">My Notes</a></h5>
                        </div> 
<!-- 
                        <div class="list-group-item list-group-item-action" id="app-options">
                            <h5 class="text-center"><a href="/talent-notes" id="talentnotes">Bookmark</a></h5>
                        </div>  -->

                        <!-- <div class="list-group-item list-group-item-action" id="app-options">
                            <h5 class="text-center">My Plans</h5>
                        </div> -->
                        @endauth                       
                    </div>                                    
    </div>                                         
</div>

<script type="text/javascript">
        (function(){
    var button = document.getElementById("info");
    var myDiv = document.getElementById("myDiv");

    function toggle() {
        if (myDiv.style.visibility === "hidden") {
            myDiv.style.visibility = "visible";
        } else {
            myDiv.style.visibility = "hidden";
        }
    }

    button.addEventListener("click", toggle, false);
        })()

    </script>



@endsection