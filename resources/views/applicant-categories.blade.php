@extends('layouts.template')
@section('title', 'Applicant Categories')
@section('content')


    <h1 class="py-5 text-center">Add Applicant Category</h1>
    <div class="container">
        <div class="row">
        <div class="col-lg-4 offset-lg-4">
                <form action="/add-category" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="name">Category:</label>
                        <input type="text" name="name" class="form-control">
                    </div>
                    <div class="text-center">
                        <button type="submit" class="btn btn-info">Add Category</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="col-lg-6 offset-lg-3">
            <h1 class="py-3 text-center">All Categories</h1>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Category</th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
            
                   
            </table>

        </div>
    </div>

@endsection