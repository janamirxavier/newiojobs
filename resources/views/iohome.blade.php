@extends('layouts.front-template')
@section('title', 'Home')
@section('content')
    
        
  
			<div class="subnav d-flex justify-content-center align-items-center">            
                <a href="#bring" class="submain mx-5">Enterprise</a>
                <a href="#bring" class="submain mx-5">Solutions</a>
                <a href="#bring" class="submain mx-5">Partners</a>
                <a href="/pricing" class="submain mx-5">Pricing</a>
                <a href="#bring" class="submain mx-5">Top Talents</a>           
			</div>
				
<div class="jumbotron jumbotron-fluid col-md-12 mr-auto" id="main-jumbo">	
	<div class = "container">
		<img src="{{ URL::to('/assets/img/test8.png') }}" alt="" class="center my-4" >
		<div class="row" >
			<p id="front-text">We Connect Clients<br>
            We Connect People<br>
			We Are IOjobs</>			
		</div>
		
	
		<div class="row" id="hire-text">
			<h5>Hire the best online Talents PH</h5>
		</div>

		<div class="row" id="front-btn">			
			<button class="btn btn-info" id="gtstrted">Get Started</button>	
		</div>	

		<div class="row" id="front-2ndbtn">			
			<button class="btn btn-secondary" id="learnbtn">Learn More</button>	
		</div>	
		

	</div>
	
</div>    
        <section class="d-flex justify-content-center align-items-center flex-column">
		<h1 class="display-4 text-center py-4" id="jumbo-text">Why IOjobs</h1>
			<div class="card-deck py-4">
				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/motivation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Community</h5>
				    </div>
                </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/bulb.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Innovation</h5>
				    </div>
                  </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/presentation.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Integrity</h5>
				    </div>
				  </div>				 
			</div>

			
			<div class="card-deck py-4">			
				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/music-new.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Coolness</h5>
				    </div>
                </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/rocket.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Awesomeness</h5>
				    </div>
                  </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/techy.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Techyness</h5>
				    </div>
				  </div>				 
        	</div>
		</section>


<div class="container-second">		
	<section class="d-flex justify-content-center align-items-center flex-column" id="">
			
        <h1 class="display-4 text-center py-4" id="jumbo-text">How It Works</h1>			
			<div class="card-deck py-4">

				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/new-registration.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Register</h5>
				    </div>
                </div>

				<div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/post.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Post a Job</h5>
				    </div>
                </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/filter2.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Filter Applicants</h5>
				    </div>
                  </div>
                  
				  <div class="card" id="main-cards">
				    <img src="{{ URL::to('/assets/img/handshake-new.png') }}" class="card-img-top" alt="...">
				    <div class="card-body">
				      <h5 class="card-title text-center">Hire Talents</h5>
				    </div>
				  </div>
				 
        	</div>

		</div>	
	</section>
</div>

@endsection