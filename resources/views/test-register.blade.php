@extends('layouts.front-template')
@section('title', 'Registration')
@section('content')


    <h1 class="text-center py-4">Client Register</h1>

<div class="container">
<div class="row">    
    
<div class="col-lg-6 offset-lg-3 card-body" id="profilecard">           
                    <form action="/register-user"  method="POST">
                        @csrf
                        <div class="form-group py-3">
                            <label for="jobtitle">Name:</label>
                            <input type="text" name="name" class="form-control">
                        </div>
                      

                        <div class="form-group py-3">
                            <label for="jobtitle">Email:</label>
                            <input type="text" name="email"  class="form-control">
                        </div>

                        <div class="form-group py-3">
                            <label for="jobtitle">Password:</label>
                            <input type="password" name="password" class="form-control">
                        </div>


                        <div class="text-center">
                            <button class="btn btn-info">Register</button>
                        </div>
                    </form>   
            </div>
       
</div>
</div>

@endsection