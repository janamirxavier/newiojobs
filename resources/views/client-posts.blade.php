@extends('layouts.template')
@section('title', 'Job Posts')
@section('content')

                <div class="container">
                <h1 class="text-center py-5 io-text">My Jobs</h1>
                    <div class="d-flex justify-content-center">
                        <form method="get" action="/client-add-jobs">
                            <!-- <i style='font-size:25px' class='far my-3 mx-2' id="add-note">&#xf271;</i>  -->
                            <button type="submit" class="btn btn-info text-center my-1">Post New Job</button>
                        </form>
                    </div>    
                </div>
      
    <div class="container my-4" id="indexContainer">
        <div class="d-flex justify-content-center">
            <div class="col-lg-10">
           
            <table class="table table-striped">
                <thead>
                    <tr>
                        <!-- <th>ID</th> -->
                        <th>JobTitle</th>
                        <th>Client</th>
                        <th>Country</th>
                        <th>Posted</th>
                        
                    </tr>
                </thead>
                <tbody>
                
                
                @foreach($clientjobs as $job)
                @auth
                @if(Auth::user()->id === $job->user_id)
                    <tr class="jobs-tr">
                        <!-- <td>{{$job->id}}</td> -->
                        <td>{{$job->jobtitle}}</td>
                        <td>{{$job->client}}</td>
                        <td> {{$job->country}} </td>
                        <td> {{$job->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}} </td>
                        <td>

                            <div class="d-flex">
                           
                            <a href="/client-update-job/{{ $job->id }}" class="btn  mr-3"><i style='font-size:24px' class='far' id="update-note">&#xf274;</i></a>
                            

                            <!-- <a href="/delete-job/{{$job->id}}" class="btn btn-danger">Delete</a> -->

                            <!-- <form action="/delete-job" method="POST">
                                @csrf
                                @method('DELETE')
                                <input type="hidden" name="job_id" value="{{$job->id}}">                             
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form> -->

                                <!-- <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal"> Delete </button> -->
                                <button type="button" class="btn" data-toggle="modal" data-target="#exampleModal"> <i style="font-size:24px" class="fa mx-2" id="delete-note" >&#xf00d;</i> </button>

                                <!-- Modal -->

                                
                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <!-- <h5 class="modal-title" id="exampleModalLabel">Modal title</h5> -->
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-center">
                                        Are you sure to delete this job post?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <a href="/delete-job/{{$job->id}}" class="btn btn-danger">Delete</a>
                                    </div>
                                    </div>
                                </div>
                                </div>

                                
                            
                            </div>
                        </td>
                    </tr> 
                @endif 
                @endauth
                @endforeach
                </tbody>
                      
            </table>
                   
            <div>
                        
                </div>                
            </div>                                                              
        </div>                    
    </div>


@endsection