<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>@yield('title')</title>

{{--    bootswatch --}}
    <link rel="stylesheet" href="https://bootswatch.com/4/spacelab/bootstrap.css">
    <!-- <link rel="stylesheet" href=https://bootswatch.com/4/cerulean/bootstrap.css> -->

    <link rel="stylesheet" href="/css/styles.css">

    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300&family=Rubik&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@300&family=Nunito+Sans:wght@200&family=Rubik&display=swap" rel="stylesheet">

    <link href="https://fonts.googleapis.com/css2?family=Varela+Round&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

</head>
<body>
    <div id="app">
    <nav class="navbar navbar-expand-lg navbar-light" id="nav-top">
    <a class="navbar-brand ml-5 mr-5">IOjobs</a>
    
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

        

    <div class="collapse navbar-collapse ml-auto" id="navbarNav">

        @auth
        @if(Auth::user()->role_id === 1)
        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/jobs">MAIN</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/job-posts">ALL JOBS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/categories">ALL CATEGORIES</a>
        </li>
        <li class="nav-item">
                    <form action="/logout" method="POST">
                        @csrf
                        <button class="btn btn-info">Logout</button>
                    </form>
        </li>
        </ul>

        
        @elseif(Auth::user()->role_id ===2)

        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/applicant-jobs">MAIN</a>
        </li>
        
        <li class="nav-item">
            <a class="nav-link" href="/applicant-profile">MY PROFILE</a>
        </li>
       
        <li class="nav-item">
            <a class="nav-link" href="">MESSAGES</a>
        </li>

        <li class="nav-item">
                    <form action="/logout" method="POST">
                        @csrf
                        <button class="btn btn-info">Logout</button>
                    </form>
        </li>
        </ul>




        @else

        <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <a class="nav-link" href="/client-jobs">MAIN</a>
        </li>
        
        <li class="nav-item">
            <a class="nav-link" href="/client-posts">MY JOBS</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="">MESSAGES</a>
        </li>

        <li class="nav-item">
                    <form action="/logout" method="POST">
                        @csrf
                        <button class="btn btn-info">Logout</button>
                    </form>
        </li>
        </ul>
      
        @endif
        @endauth
    

    </div>
    </nav>
   </div> 

    @yield('content')

    <footer class="py-5 mt-5" id="footer-part">
        <div class="d-flex justify-content-center align-items-center">
            <ul style="list-style-type:none;">
                <h4>About Us</h4>
                <li>Blogs</li>
                <li>Community</li>
            </ul> 

            <ul style="list-style-type:none;">
                <h4>Terms of Service</h4>
                <li>Privacy Policy</li>
                <li>Accessibility</li>
            </ul>

            <ul style="list-style-type:none;">
                <h4>Trust, Safety & Security</h4>
                <li>Help & Support</li>
                <li>Our Team</li>
            </ul>

            <ul style="list-style-type:none;">
                <h4>Desktop App</h4>
                <li>Cookie Policy</li>
                <li>Enterprise Solutions</li>
            </ul>


        </div>

            <div class="d-flex justify-content-center align-items-center py-3" id="social-list">
				 <a href="https://www.facebook.com/" class="soc-logo" a target=”_blank”>
					<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-fb.png') }}">
				</a>

				<a href="https://www.twitter.com/" class="soc-logo" a target=”_blank>
				<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-twitter.png') }}">
				</a>

				<a href="https://www.instagram.com/" class="soc-logo" a target=”_blank>
				<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-linkedin.png') }}">
				</a>
			</div>	
				
				<p class="text-center text-white my-2" id="copy-text">Copyright &copy; IOjobs 2020</p>
          
    </footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="sweetalert2.all.min.js"></script>
<!-- Optional: include a polyfill for ES6 Promises for IE11 -->
<script src="https://cdn.jsdelivr.net/npm/promise-polyfill"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>