<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
    <title>@yield('title')</title>

{{--    bootswatch --}}
    <link rel="stylesheet" href="https://bootswatch.com/4/spacelab/bootstrap.css">
    <!-- <link rel="stylesheet" href=https://bootswatch.com/4/cerulean/bootstrap.css> -->

    <link rel="stylesheet" href="/css/styles.css">
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'>
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


</head>
<body>
    <div id="app">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('iohome') }}">
                    IOjobs
                </a>
                
                

                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                   

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        
                        @guest
                        
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif -->

                            <ul class="navbar-nav mr-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Register
                                </a>
                                <div class="dropdown-menu" aria-labelledby="navbarDropdown1">
                                    <a class="dropdown-item" href="{{ url('/client-register') }}">As Client</a>
                                    <a class="dropdown-item" href="{{ url('/applicant-register') }}">As Applicant</a>
                                    <!-- <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                    </div> -->
                            </li>
                            </ul>
                            

                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>

                         

                        @endguest
                    </ul>
                </div>
            </div>

            
        </nav>
           
              
</div> 

    @yield('content')

    <footer class="py-5 mt-5" id="footer-part">
        <div class="d-flex justify-content-center align-items-center">
            <ul style="list-style-type:none;">
                <h4>About Us</h4>
                <li>Blogs</li>
                <li>Community</li>
            </ul> 

            <ul style="list-style-type:none;">
                <h4>Terms of Service</h4>
                <li>Privacy Policy</li>
                <li>Accessibility</li>
            </ul>

            <ul style="list-style-type:none;">
                <h4>Trust, Safety & Security</h4>
                <li>Help & Support</li>
                <li>Our Team</li>
            </ul>

            <ul style="list-style-type:none;">
                <h4>Desktop App</h4>
                <li>Cookie Policy</li>
                <li>Enterprise Solutions</li>
            </ul>


        </div>

            <div class="d-flex justify-content-center align-items-center py-3" id="social-list">
				 <a href="https://www.facebook.com/" class="soc-logo" a target=”_blank”>
					<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-fb.png') }}">
				</a>

				<a href="https://www.twitter.com/" class="soc-logo" a target=”_blank>
				<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-twitter.png') }}">
				</a>

				<a href="https://www.instagram.com/" class="soc-logo" a target=”_blank>
				<img class="social-logo mx-2" src="{{ URL::to('/assets/img/new-linkedin.png') }}">
				</a>
			</div>	
				
				<p class="text-center text-white my-2" id="copy-text">Copyright &copy; IOjobs 2020</p>
          
    </footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>