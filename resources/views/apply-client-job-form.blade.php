@extends('layouts.template')
@section('title', 'Apply Job')
@section('content')
    <!-- <h1 class="text-center py-5">Apply Job</h1> -->
    <div class="py-5 container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
            <div class="app-form card-body margin-bottom" id="apply-job-card">
                <form action="/apply-client-job/{{$clientjobs->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    
                    <div class="form-group">
                        <h3 class="card-title text-center my-5" >{{$clientjobs->jobtitle}}</h3>
                        
                    </div>
                    <div class="form-group">
                        <label for="jobtitle">Client: {{$clientjobs->client}}</label>
                        
                    </div>
                    <div class="form-group">
                        <label for="jobdescription">Description: {{$clientjobs->jobdescription}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary: {{$clientjobs->salary}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="country">Country: {{$clientjobs->country}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="category">Category: {{$clientjobs->category->name}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="category">Posted: {{$clientjobs->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}}</label>  
                    </div>

                    <!-- <div class="form-group">
                        <label for="category">User: {{$clientjobs->user_id}}</label>  
                    </div> -->

                    <div class="form-group">
                        <label for="message">Message:</label>
                        <!-- <input type="text-area" name="" value="" class="form-control"> -->

                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="6"></textarea>
                    </div>


                    <div class="text-center">
                        <button class="btn btn-info">Apply Job</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection