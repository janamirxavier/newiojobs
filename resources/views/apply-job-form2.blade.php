@extends('layouts.template')
@section('title', 'Update Job')
@section('content')
    <h1 class="text-center py-5">Apply Job</h1>
    <div class="container">
        <div class="row">
            <div class="col-lg-4 offset-lg-4">
            <div class="app-form card-body margin-bottom" id="apply-job-card">
                <form action="/apply-job/{{$jobs->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                    <div class="form-group">
                        <label for="jobtitle">Job Title: {{$jobs->jobtitle}}</label>
                        
                    </div>
                    
                    <div class="form-group">
                        <label for="jobdescription">Description: {{$jobs->jobdescription}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="salary">Salary: {{$jobs->salary}}</label>
                       
                    </div>
                    <div class="form-group">
                        <label for="country">Country: {{$jobs->country}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="country">Category: {{$jobs->category_id}}</label>                       
                    </div>

                    <div class="form-group">
                        <label for="message">Message:</label>
                        <input type="text-area" name="" value="" class="form-control">
                    </div>


                    <div class="text-center">
                        <button class="btn btn-info">Apply Job</button>
                    </div>
                </form>
            </div>
            </div>
        </div>
    </div>
@endsection