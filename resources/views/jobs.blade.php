@extends('layouts.template')
@section('title', 'Jobs')
@section('content')

<h1 class="text-center py-3 io-text">IO Jobs</h1>
<div class="container">
        <div class="row">
                    <div class="col-lg-2">
                        <!-- <h4 class="text-center filter-text">Filter</h4> -->
                        <div class="list-group">
                                <a href="/jobs" class="all-filter text-center">All</a>
                            @foreach($categories as $category)
                                <a href="/jobs?category_id={{$category->id}}" class="list-group-item list-group-item-action" id="categories">{{$category->name}}</a>
                            @endforeach
                        </div>
                    </div>    

                    <div class="col-lg-8">
                            <div class="container">
                                <div class="row">
                                    @foreach($jobs as $job)
                                    <div class="col-lg-10" >
                                        <div class="card border border-light">
                                            <div class="card-body margin-bottom">
                                         
                                            <h3 class="card-title text-center my-3" >{{$job->jobtitle}}</h3>
                                            
                                                <p class="card-text">Job Description: {{$job->jobdescription}}</p>
                                                <p class="card-text">Salary: {{$job->salary}}</p>
                                                <p class="card-text">Country: {{$job->country}}</p>
                                                <p class="card-text">Category: {{$job->category->name}}</p>
                                                <p class="card-text">Posted: {{$job->created_at}}</p>

                                      

                                                <div class="d-flex">
                                                <a href="/update-job/{{ $job->id }}" class="btn btn-info mr-3">Update</a> 
                                                <form action="/delete-job" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <input type="hidden" name="job_id" value="{{$job->id}}">                             
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>                                                     
                                                </div>

                                               
                                                
                                            </div>
                                             
                                            </div>
                                        </div>
                                    @endforeach

                                    @foreach($clientjobs as $job)
                                    <div class="col-lg-10" >
                                        <div class="card border border-light">
                                            <div class="card-body margin-bottom">
                                            
                                            <p class="card-text text-center" id="vip">{{$job->vip}}</p>
                                            <h3 class="card-title text-center my-3" >{{$job->jobtitle}}</h3>
                                       
                                                <p class="card-text">Client: {{$job->client}}</p>
                                                <p class="card-text">Job Description: {{$job->jobdescription}}</p>
                                                <p class="card-text">Salary: {{$job->salary}}</p>
                                                <p class="card-text">Country: {{$job->country}}</p>
                                                <p class="card-text">Category: {{$job->category->name}}</p>
                                                <p class="card-text">Posted: {{$job->created_at->isoFormat('dddd, MMMM Do YYYY, h:mm')}}</p>

                                                
                                            </div>
                                             
                                            </div>
                                        </div>
                                    @endforeach 
                                                                                                       
                                    </div>
                                  
                            </div>                            
                        </div> 
                    <div>


                    


            </div>                                    
        </div>                                                                      
    </div>                                         
</div>
@endsection