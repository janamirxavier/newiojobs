<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TalentNote extends Model
{
    public function user(){
        return $this->belongsTo('App\User');
    }
}
