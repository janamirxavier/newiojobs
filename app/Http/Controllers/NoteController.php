<?php

namespace App\Http\Controllers;

use App\User;
use App\Note;
use Illuminate\Http\Request;

use Auth;
use Session;

class NoteController extends Controller
{
    public function newnotepost(){

        // $clientjobs = Clientjob::all();
        // $categories = Category::all();
        // return view('client-posts', compact('clientjobs','categories'));

        $notes = Note::all();
        return view('client-notes', compact('notes'));
    }
}
