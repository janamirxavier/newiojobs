<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;




use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class TestRegisterController extends Controller
{
    public function testindex(){

     
        return view('test-register');
        
    }

    public function saveuser(Request $request){

        $users = new User;
        $users->name = $request->name;
        $users->email = $request->email;
        $users->password = $request->password;
        $users->role_id = $request->role_id=3;

        $users->save();
     
        return back();
        
    }

}
