<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Applicant;
use App\User;
use App\Category;
use App\Job;
use App\Clientjob;

use Auth;
use Session;


class JobAppController extends Controller
{
   
    public function index(){

        $jobs = Job::all();
        $categories = Category::all();

        return view('jobs', compact('jobs'), compact('categories'));
        
    }




    public function edit($id)
    {
        $jobs = job::find($id);
        $categories = Category::all();

        return view('apply-job-form', compact('jobs'), compact('categories'));
    }

    public function editclient($id)
    {
        
        $clientjobs = Clientjob::find($id);
        $categories = Category::all();

        return view('apply-client-job-form', compact('clientjobs'), compact('categories'));
    }

    public function editclientnew($id)
    {
        
        $clientjobs = Clientjob::find($id);
        $categories = Category::all();

        return view('applicant-feedback-job-form', compact('clientjobs'), compact('categories'));
    }

    public function testsendMail(Request $request){
        $this->validate($request, [
            'email' => 'required',
            'subject' => 'required',
            'content' => 'required',
        ]);

        $myMail = new MyMail(
            $request->input('subject'),
            $request->input('content')
        );

        Mail::to($request->input('email'))->send($myMail);
        return redirect()->back()->with('success','Email sent successfully to:' . $request->input('email'));
    }

  
}
