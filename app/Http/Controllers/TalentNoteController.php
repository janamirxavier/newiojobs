<?php

namespace App\Http\Controllers;

use App\User;
use App\TalentNote;
use Illuminate\Http\Request;

use Auth;
use Session;

class TalentNoteController extends Controller
{
    public function notepost(){

        // $clientjobs = Clientjob::all();
        // $categories = Category::all();
        // return view('client-posts', compact('clientjobs','categories'));

        $talentnotes = TalentNote::all();
        return view('talent-notes', compact('talentnotes'));

        
    }

    public function clientnotepost(){

        // $clientjobs = Clientjob::all();
        // $categories = Category::all();
        // return view('client-posts', compact('clientjobs','categories'));

        $talentnotes = TalentNote::all();
        return view('client-notes', compact('talentnotes'));
    }

    public function clientcreate()
    {
  
        return view('client-add-notes');

    }

    public function clientstore(Request $request){

        $talentnotes = new TalentNote;
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
        $talentnotes->user_id = Auth::user()->id;

        $talentnotes->save();

        return redirect('client-notes');
    }

    public function clientedit($id){

        $talentnotes = TalentNote::find($id);
        
        return view('talent-notes-update2', compact('talentnotes'));
    
    }

    public function clientupdate(Request $request){

        $talentnotes = TalentNote::find($request->id);
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
    
        $talentnotes->save();

        return redirect('client-notes');
    }

    public function clientdestroy($id)
    {   
        $talentnotes = TalentNote::find($id);
       
        $talentnotes->delete();

        return back();
    }

  

    public function create()
    {
  
        return view('talent-add-notes');

    }

    

    public function store(Request $request){

        $talentnotes = new TalentNote;
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
        $talentnotes->user_id = Auth::user()->id;

        $talentnotes->save();

        return redirect('talent-notes');
    }

    public function edit($id){

        $talentnotes = TalentNote::find($id);
        
        return view('talent-notes-update', compact('talentnotes'));
    
    }

    public function update(Request $request){

        $talentnotes = TalentNote::find($request->id);
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
    
        $talentnotes->save();

        return back();
    }
  

    public function destroy($id)
    {   
        $talentnotes = TalentNote::find($id);
       
        $talentnotes->delete();

        return back();
    }
}
