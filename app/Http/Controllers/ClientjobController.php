<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Applicant;
use App\User;
use App\Category;
use App\Job;
use App\Clientjob;
use Alert;
use Auth;
use Session;

class ClientjobController extends Controller
{
    public function index(Request $request){

        $jobs = Job::all();
        $clientjobs = Clientjob::all();
        if(isset($request->category_id)){
            $clientjobs=Clientjob::where('category_id',$request->category_id)->get();
        }
        $categories = Category::all();

        return view('client-jobs', compact('jobs','clientjobs','categories'));

    }

    public function jobpost(){

        $clientjobs = Clientjob::all();
        $categories = Category::all();

        return view('client-posts', compact('clientjobs','categories'));
        
    }

    public function create()
    {
        $categories = Category::all();

        return view('client-add-jobs', compact('categories'));

    }

    public function store(Request $request){

        $clientjobs = new Clientjob;
        $clientjobs->vip = $request->vip;
        $clientjobs->client = $request->client;
        $clientjobs->jobtitle = $request->jobtitle;
        $clientjobs->jobdescription = $request->jobdescription;
        $clientjobs->salary = $request->salary;
        $clientjobs->country = $request->country;
        $clientjobs->category_id = $request->category_id;
        $clientjobs->user_id = Auth::user()->id;

        $clientjobs->save();

        return redirect('client-posts');
    }


    public function destroy($id)
    {   
        $clientjobs = Clientjob::find($id);
       
        $clientjobs->delete();

        Alert::html('Are you sure you want to remove this item?', "<a href='/remove-cart-item/$itemId' class='btn btn-danger'>Remove</a>", 'question');
        return back();
    }

    public function edit($id)
    {
        $clientjobs = Clientjob::find($id);
        $categories = Category::all();

        return view('client-update-job-form', compact('clientjobs'), compact('categories'));
    }


    public function update($id, Request $request)
    {
            $clientjobs = Clientjob::find($id);
            $clientjobs->client = $request->client;
            $clientjobs->jobtitle = $request->jobtitle;
            $clientjobs->jobdescription = $request->jobdescription;
            $clientjobs->salary = $request->salary;
            $clientjobs->country = $request->country;
            $clientjobs->category_id = $request->category_id;

            $clientjobs->save();
            
            return redirect('/client-posts');
    }
}
