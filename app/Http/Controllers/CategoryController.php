<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();

        return view('categories', compact('categories'));
    }

    public function store(Request $request)
    {

        $new_category = new Category;
        $new_category->name = $request->name;
        $new_category->save();

        return redirect('/categories');
    }

    public function edit($id)
    {

        $categories = Category::find($id);

        return view('update-category-form', compact('categories'));

        // return view('update-category-form');

    }

    public function update($id, Request $request)
    {

        $categories = Category::find($id);
        $categories->name = $request->name;
    
        $categories->save();

        // dd($categories);    
        return redirect('categories');
        
    }
    

    public function destroy(Request $request)
    {

        $id = $request->category_id;
        $categories = Category::find($id);
        $categories->delete();

        return back();

    } 
}
