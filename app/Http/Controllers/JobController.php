<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Job;
use App\Clientjob;
use Auth;
use Session;


class JobController extends Controller
{
    public function mainindex(){

        return view('iohome');
        
    }

    public function pricingindex(){

        return view('pricing');
        
    }

    
    public function index( Request $request){

        $jobs = Job::all();
        $clientjobs = Clientjob::all();
        if(isset($request->category_id)){
            $clientjobs=ClientJob::where('category_id',$request->category_id)->get();
        }
        $categories = Category::all();

        return view('jobs', compact('jobs','clientjobs','categories'));
        
    }

    public function jobpost(){

        $jobs = Job::all();
        $clientjobs = Clientjob::all();
        $categories = Category::all();

        return view('job-posts', compact('jobs','clientjobs','categories'));
        
    }

    public function create()
    {
        $categories = Category::all();

        return view('add-jobs', compact('categories'));

    }

    public function store(Request $request){

        $clientjobs = new Clientjob;
        $clientjobs->client = $request->client;
        $clientjobs->jobtitle = $request->jobtitle;
        $clientjobs->jobdescription = $request->jobdescription;
        $clientjobs->salary = $request->salary;
        $clientjobs->country = $request->country;
        $clientjobs->vip = $request->vip;
        $clientjobs->category_id = $request->category_id;
        $clientjobs->user_id = Auth::user()->id;

        $clientjobs->save();

        return redirect('jobs');
    }

    public function vipcreate()
    {
        $categories = Category::all();
        return view('add-vip-jobs', compact('categories'));

    }

    public function vipstore(Request $request){

        $clientjobs = new Clientjob;
        $clientjobs->client = $request->client;
        $clientjobs->jobtitle = $request->jobtitle;
        $clientjobs->jobdescription = $request->jobdescription;
        $clientjobs->salary = $request->salary;
        $clientjobs->country = $request->country;
        $clientjobs->vip = $request->vip;
        $clientjobs->category_id = $request->category_id;
        $clientjobs->user_id = Auth::user()->id;

        $clientjobs->save();

        return redirect('jobs');
    }
    
    


    public function destroy(Request $request)
    {
        $id = $request->job_id;
        $clientjobs = Clientjob::find($id);
        $clientjobs->delete();

        return back();
    }

    public function edit($id)
    {
        $clientjobs = Clientjob::find($id);
        $categories = Category::all();

        return view('update-job-form', compact('clientjobs'), compact('categories'));
    }


    public function update($id, Request $request)
    {
            $clientjobs = Clientjob::find($id);
            $clientjobs->client = $request->client;
            $clientjobs->jobtitle = $request->jobtitle;
            $clientjobs->jobdescription = $request->jobdescription;
            $clientjobs->salary = $request->salary;
            $clientjobs->country = $request->country;
            $clientjobs->category_id = $request->category_id;

            $clientjobs->save();
            
            return redirect('/jobs');
    }

}
