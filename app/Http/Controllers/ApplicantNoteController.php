<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\ApplicantNote;
use App\Profile;

use Auth;
use Session;


class ApplicantNoteController extends Controller
{
    public function notepost(){

        // $clientjobs = Clientjob::all();
        // $categories = Category::all();
        // return view('client-posts', compact('clientjobs','categories'));

        $talentnotes = TalentNote::all();
        return view('talent-notes', compact('talentnotes'));

        
    }

  

    public function create()
    {
  
        return view('talent-add-notes');

    }

    public function store(Request $request){

        $talentnotes = new TalentNote;
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
        $talentnotes->user_id = Auth::user()->id;

        $talentnotes->save();

        return redirect('talent-notes');
    }

    public function edit($id){

        $talentnotes = TalentNote::find($id);
        
        return view('talent-notes-update', compact('talentnotes'));
    
    }

    public function update(Request $request){

        $talentnotes = TalentNote::find($request->id);
        $talentnotes->title = $request->title;
        $talentnotes->content = $request->content;
    
        $talentnotes->save();

        return redirect('talent-notes');
    }
  

    public function destroy($id)
    {   
        $talentnotes = TalentNote::find($id);
       
        $talentnotes->delete();

        return back();
    }
    
}
