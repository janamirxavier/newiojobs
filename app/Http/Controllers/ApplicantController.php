<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\User;
use App\Category;
use App\Job;
use App\Clientjob;
use App\Profile;

use Auth;
use Session;


class ApplicantController extends Controller
{
    public function index(Request $request){

        
        $clientjobs = Clientjob::all();
        if($request->category_id){
            $clientjobs=Clientjob::where('category_id',$request->category_id)->get();
        }
        $categories = Category::all();
        dd($request->category_id);

        return view('applicant-jobs', compact('clientjobs','categories'));

    }


    public function profileindex(Request $request){

        $applicant_users = User::all();        
        $profile = new Profile;
       
        return view('applicant-profile', compact('applicant_users'));
    
        }

    public function store(Request $request){

        $applicant_users = User::all();
        $profile = new Profile;
        $profile->address = $request->address;
        $profile->contact = $request->contact;
        $profile->links = $request->links;
        $profile->summary = $request->summary;
        $profile->imgPath = $request->imgPath;
        $profile->save();
           
        return view('applicant-profile', compact('applicant_users'));
        
        }

    public function updateprofileindex(Request $request){

        $applicant_users = User::all();        
        $profile = new Profile;
       
        return view('applicant-update-profile', compact('applicant_users'));
    
        }
    
}
