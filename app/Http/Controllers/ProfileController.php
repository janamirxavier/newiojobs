<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Applicant;
use App\User;
use App\Category;
use App\Job;
use App\Clientjob;
use App\Profile;
use App\ApplicantNote;

use Auth;
use Session;


class ProfileController extends Controller
{
    public function index(Request $request){

        $jobs = Job::all();
        $clientjobs = Clientjob::all();
        if(isset($request->category_id)){
            $clientjobs=ClientJob::where('category_id',$request->category_id)->get();
        }
        $categories = Category::all();

        return view('applicant-jobs', compact('jobs','clientjobs','categories'));

    }


    public function profileindex(Request $request){
        $count = Profile::where('user_id', Auth::user()->id)->count();
        $profiles = Profile::where('user_id', Auth::user()->id)->get();
        $applicant_users = User::all();        
      
       
        return view('applicant-profile', compact('applicant_users', 'count', 'profiles'));
    
        }

        public function profileedit(Request $request)
        {
            $profiles = Profile::find($id);
    
            return view('applicant-edit-profile', compact('profiles'));
        }

        public function updatephotoindex($id){
            $profile = Profile::find($id);
            $applicant_users = User::all();        
           
            return view('applicant-update-photo', compact('applicant_users', 'profile'));
        
            }
            
            public function updatephotostore(Request $request){

                
                $profile = Profile::find($request->id);
                            
                    
                $image = $request->file('imgPath');
                $image_name = time() . "." . $image->getClientOriginalExtension();
                $destination = "images/";
                $image->move($destination, $image_name);
                $profile->imgPath = $destination . $image_name;

                $profile->save();
                  
                return redirect('/applicant-profile');                
    }   
   

        public function updateprofileindex($id){
            $profile = Profile::find($id);
            $applicant_users = User::all();        
           
            return view('applicant-update-profile', compact('applicant_users', 'profile'));
        
            }

            
            public function updateprofilestore(Request $request){

                
                $profile = Profile::find($request->id);
                $profile->address = $request->address;
                $profile->contact = $request->contact;
                $profile->links = $request->links;
                $profile->summary = $request->summary;
                
                    
       

                $profile->save();


                   
                return redirect('/applicant-profile');                
    }     

  
    

    public function addProfile(Request $request){
        $profile = new Profile;
        $profile->user_id = $request->user_id;
        $profile->address = $request->address;
        $profile->contact = $request->contact;
        $profile->links = $request->links;
        $profile->summary = $request->summary;
   
            
        $image = $request->file('imgPath');
        $image_name = time() . "." . $image->getClientOriginalExtension();
        $destination = "images/";
        $image->move($destination, $image_name);
        $profile->imgPath = $destination . $image_name;

        $profile->save();

        return redirect('/applicant-profile');

    }

    public function add(){
        return view('applicant-add-profile');
    }

}
