<?php

namespace App\Http\Controllers;


use App\User;
use App\NoteClient;
use Illuminate\Http\Request;

use Auth;
use Session;

class NoteClientController extends Controller
{
    public function clientnotepost(){

        // $clientjobs = Clientjob::all();
        // $categories = Category::all();
        // return view('client-posts', compact('clientjobs','categories'));

        $clientnotes = NoteClient::all();
        return view('client-notes', compact('clientnotes'));
    }
}
