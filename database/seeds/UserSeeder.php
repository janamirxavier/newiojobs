<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert(array(
            0 =>
            array(
            'name' => 'Admin One',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'role_id' => 1,
            ),
            1 =>
            array(
            'name' => 'Applicant One',
            'email' => 'applicant@applicant.com',
            'password' => Hash::make('password'),
            'role_id' => 2,
            ),
        ));
    }
}