<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRolesToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('role_id')->default(1);
            $table->foreign('role_id')
                ->on('roles')
                ->references('id')
                ->onUpdate('cascade')
                ->onDelete('cascade');

                
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //since we added a foreign key
            //we need to remove the foreign key constraint
            //and the foreign key itself
            $table->dropForeign(['role_id']);
            $table->dropColumn('role_id');
        });
    }
}
