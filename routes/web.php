<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::middleware("admin")->group(function(){
    
    Route::get('/jobs', 'JobController@index');
    Route::get('/job-posts', 'JobController@jobpost');
    Route::get('/add-jobs', 'JobController@create');
    Route::post('/add-jobs', 'JobController@store');
    Route::get('/add-vip-jobs', 'JobController@vipcreate');
    Route::post('/add-vip-jobs', 'JobController@vipstore');
    Route::get('/update-job/{id}', 'JobController@edit');
    Route::patch('/update-job/{id}', 'JobController@update');
    Route::post('/delete-job-admin', 'JobController@destroy');
    Route::get('/categories', 'CategoryController@index');
    Route::post('/add-category', 'CategoryController@store');
    Route::get('/update-category/{id}', 'CategoryController@edit');
    Route::patch('/update-category/{id}', 'CategoryController@update');
    Route::delete('/delete-category', 'CategoryController@destroy');
    
});

Route::middleware("client")->group(function(){
    Route::get('/client-jobs', 'ClientjobController@index');
    Route::get('/client-posts', 'ClientjobController@jobpost');
    Route::get('/client-add-jobs', 'ClientjobController@create');
    Route::post('/client-add-jobs', 'ClientjobController@store');
    Route::get('/client-update-job/{id}', 'ClientjobController@edit');
    Route::patch('/client-update-job/{id}', 'ClientjobController@update');
    Route::get('/delete-job/{id}', 'ClientjobController@destroy');


    Route::get('/client-notes', 'TalentNoteController@clientnotepost');


    Route::get('/client-add-notes', 'TalentNoteController@clientcreate');

    Route::post('/client-add-notes', 'TalentNoteController@clientstore');

    Route::get('/talent-notes-update2/{id}', 'TalentNoteController@clientedit');
    Route::patch('/talent-notes-update2/{id}', 'TalentNoteController@clientupdate');

    Route::get('/delete-client-note/{id}', 'TalentNoteController@clientdestroy');
 

});


Route::middleware("applicant")->group(function(){

    Route::get('/applicant-jobs', 'ProfileController@index');
    Route::post('/applicant-jobcategory', 'ProfileController@store');

    Route::get('/applicant-profile', 'ProfileController@profileindex');
    Route::post('/applicant-profile', 'ProfileController@store');
    Route::get('/applicant-add-profile', 'ProfileController@add');
    Route::post('/applicant-add-profile', 'ProfileController@addProfile');

    Route::get('/applicant-update-profile/{id}', 'ProfileController@updateprofileindex');
    Route::post('/applicant-update-profile', 'ProfileController@updateprofilestore');

    Route::get('/applicant-update-photo/{id}', 'ProfileController@updatephotoindex');
    Route::post('/applicant-update-photo', 'ProfileController@updatephotostore');
    

    Route::get('/applicant-edit-profile', 'ProfileController@profileedit');
    Route::post('/applicant-edit-profile/{id}', 'ProfileController@profileupdate');

    Route::get('/applicant-add-notes', 'ApplicantNoteController@createnote');
    Route::post('/applicant-add-notes', 'ApplicantNoteController@storenote');


    Route::get('/talent-notes', 'TalentNoteController@notepost');
    Route::get('/talent-notes-update/{id}', 'TalentNoteController@edit');
    Route::patch('/talent-notes-update/{id}', 'TalentNoteController@update');


    Route::get('/talent-add-notes', 'TalentNoteController@create');
    Route::post('/talent-add-notes', 'TalentNoteController@store');

    Route::get('/delete-talent-note/{id}', 'TalentNoteController@destroy');

    // Route::get('/talent-update-note', 'TalentNoteController@edit');

    Route::get('/test-update', 'TalentNoteController@test');


    Route::get('/applicant-notes', 'ApplicantNoteController@notepost');

    Route::get('/apply-job/{id}', 'JobAppController@edit');
    Route::get('/apply-client-job/{id}', 'JobAppController@editclient');

    Route::get('/applicant-feedback-job-new/{id}', 'JobAppController@editclientnew');


    Route::post('/applicant-feedback-job-new', 'JobAppController@testsendMail');


    Route::patch('/apply-job/{id}', 'JobAppController@update');

    Route::get('/test-mail', 'MyMailController@testmailindex');
    Route::post('/test-mail', 'MyMailController@testsendMail');

    Route::get('/applicant-notes', 'ApplicantNoteController@index');
    

});

Route::get('/iohome', 'JobController@mainindex');
Route::get('/pricing', 'JobController@pricingindex');
Route::get('/client-register', 'ClientRegisterController@index');
Route::get('/applicant-register', 'ClientRegisterController@applicantindex');

Route::get('/test-register', 'TestRegisterController@testindex');
Route::post('/register-user', 'TestRegisterController@saveuser');


Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::post('/', 'MyMailController@sendMail');



